package com.slowfish.aim.userlogin.exception;

public class NickNameSetException extends Exception {
    public NickNameSetException(String name) {
        super("NickName set exception [ " + name + " ]");
    }
}
