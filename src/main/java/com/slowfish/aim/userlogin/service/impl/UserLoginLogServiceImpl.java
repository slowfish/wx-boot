package com.slowfish.aim.userlogin.service.impl;

import com.slowfish.aim.userlogin.exception.NoSuchUserException;
import com.slowfish.aim.userlogin.model.User;
import com.slowfish.aim.userlogin.service.UserLoginLogService;
import com.slowfish.aim.userlogin.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class UserLoginLogServiceImpl implements UserLoginLogService {
    @Resource
    private UserService userService;

    @Override
    public void updateLog(Long uid, String ip, Date time) {
        try {
            User ul = userService.findOne(uid);
            ul.setLastLoginIp(ip);
            ul.setLastLoginTime(time);
            userService.save(ul);
        } catch (NoSuchUserException e) {
            // do nothing
        }
    }
}
