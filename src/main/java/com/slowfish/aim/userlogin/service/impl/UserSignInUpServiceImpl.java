package com.slowfish.aim.userlogin.service.impl;

import com.slowfish.aim.userlogin.exception.NickNameSetException;
import com.slowfish.aim.userlogin.exception.NoSuchUserException;
import com.slowfish.aim.userlogin.exception.PasswordSetException;
import com.slowfish.aim.userlogin.exception.PhoneNumberHasUsedException;
import com.slowfish.aim.userlogin.model.User;
import com.slowfish.aim.userlogin.model.UserAuthRole;
import com.slowfish.aim.userlogin.repository.UserAuthRoleRepository;
import com.slowfish.aim.userlogin.service.ThirdPartTokenService;
import com.slowfish.aim.userlogin.service.UserService;
import com.slowfish.aim.userlogin.service.UserSignInUpService;
import com.slowfish.aim.utils.AppAuthFlushService;
import com.slowfish.aim.utils.MD5Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserSignInUpServiceImpl implements UserSignInUpService {
    @Resource
    private UserService userService;
    @Resource
    UserAuthRoleRepository userAuthRoleRepository;
    @Autowired
    private AppAuthFlushService appAuthFlushService;
    @Autowired
    MD5Service md5Service;
    @Value("${aim.wxboot.default_headicon}")
    private String headIcon;


    /**
     * @param name
     * @param phone
     * @param password  始末可以为空格，长度大于等于 6 即可【强制】
     */
    public void signUp(String name, String phone, String password) throws PasswordSetException,
            NickNameSetException,
            PhoneNumberHasUsedException{
        // 规整化字符串
        if (name == null) throw new NickNameSetException(name);
        name = name.trim();
        if (phone == null) throw new PhoneNumberHasUsedException(phone);
        phone = phone.trim();
        if (password == null || password.length() < 6) throw new PasswordSetException(password);

        initAccount(true, name, null, phone, password, headIcon);
        //...
        // success
    }

    @Override
    public User signUpWithThirdPart(ThirdPartTokenService.TYPE type, String openId, String nickname, String headIconUrl) {
        String credential = "defaultPassword";
        return initAccount(true, nickname, openId, null, credential, headIconUrl);
    }

    /**
     * @param signup      是否自动登陆
     * @param nickname
     * @param openId      如果是第三方登录
     * @param phone
     * @param credential
     * @param headIconUrl
     * @return
     * @throws PhoneNumberHasUsedException
     */
    private User initAccount(boolean signup, String nickname, String openId, String phone, String credential, String headIconUrl) {
        if (headIconUrl == null) headIconUrl = headIcon;
        if (nickname == null) nickname = "";
        if (credential == null) credential = "defaultPassword";
//        User ul = userRepository.findByPhone(phone);
//        if (null != ul)
//            throw new PhoneNumberHasUsedException(phone);
        User userLogin = new User();
        int length = nickname.length();
        if (length > 20) length = 20;
        userLogin.setNickname(nickname.substring(0, length));
        userLogin.setThirdPartName(openId);
        userLogin.setPhone(phone);
        userLogin.setPassword(md5Service.encode(credential));
        userLogin.setHeadIconUrl(headIconUrl);
        // 直接添加，状态为 1【正常用户】
        userLogin.setMemberStatus(User.USER_STATUS.OK);
        // 添加权限 //
        List<UserAuthRole> roles = new ArrayList<>();
        UserAuthRole userAuthRole = userAuthRoleRepository.findByName("ROLE_USER");
        roles.add(userAuthRole);
        userLogin.setRoles(roles);
        User savedUser = userService.save(userLogin);
        // 更新昵称
        if (nickname == null || nickname.trim().length() <= 1) {
            userLogin.setNickname("user_" + userLogin.getUid());
            savedUser = userService.save(userLogin);
        }
        // 添加用户基本信息，如果有 user_info 表的话 //
        //...
        // 自動登陸
        if (signup) appAuthFlushService.flush(savedUser.getUid());
        return savedUser;
    }

    /**
     * 注销用户，状态置为 -1
     *
     * @param uid
     * @return
     */
    public void writtenOff(Long uid) throws NoSuchUserException {
        User user = userService.findOne(uid);
        user.setMemberStatus(User.USER_STATUS.DISTORY);
        userService.save(user);
    }
}
