package com.slowfish.aim.userlogin.service.impl;

import com.slowfish.aim.userlogin.exception.NoSuchUserException;
import com.slowfish.aim.userlogin.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Resource
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        try {
            return userService.findByPhone(s);
        } catch (NoSuchUserException e) {
            return null;
        }
    }
}
