package com.slowfish.aim.userlogin.service;

import com.slowfish.aim.userlogin.exception.NoSuchUserException;
import com.slowfish.aim.userlogin.model.User;

public interface UserService {
    User findOne(Long uid) throws NoSuchUserException;

    User findByPhone(String phone) throws NoSuchUserException;

    User findByNickName(String nickname) throws NoSuchUserException;

    User findByThirdPartUserName(String username) throws NoSuchUserException;

    User add(User user);

    User save(User user);

    /**
     * 判断空值
     *
     * @param user
     * @return
     */
    User insertSelective(User user) throws NoSuchUserException;

    void delete(Long uid) throws NoSuchUserException;
}
