package com.slowfish.aim.userlogin.repository;

import com.slowfish.aim.userlogin.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByPhone(@Param("phone") String phone);

    User findByThirdPartName(@Param("username") String username);

    User findByNickname(@Param("nickname") String nickname);
}