package com.slowfish.aim.userlogin.repository;

import com.slowfish.aim.userlogin.model.UserAuthRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface UserAuthRoleRepository extends JpaRepository<UserAuthRole, Integer> {
    UserAuthRole findByName(@Param("name") String name);
}
