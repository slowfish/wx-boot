package com.slowfish.aim.utils;

public interface AppAuthFlushService {

    void flush(Long uid);
}
