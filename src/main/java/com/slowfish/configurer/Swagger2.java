package com.slowfish.configurer;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * Created by neoxiaoyi.
 * User: neo
 * Date: 09/12/2017
 * Time: 4:32 AM
 * Describe:
 */

@Configuration
@EnableSwagger2
public class Swagger2 {

    @Bean
    public Docket api() {
        Predicate<RequestHandler> selectors = null;
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("API接口文档")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.slowfish.apis"))
//                .paths(PathSelectors.ant("/api/v1/**"))
                .build();
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("AIM Api Document")
                .description("AIM API 文档")
                .version("1.0")
//                .termsOfServiceUrl("http://art.seeuio.com/api/")
                .build();
    }

}
