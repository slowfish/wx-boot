package com.slowfish.configurer.httpstatus;

import com.slowfish.core.R;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HttpUnauthorizedController {

    @RequestMapping("/unauthorized")
    public ResponseEntity unauthorizedPage() {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(R.code(401).message("无权访问 [unauthorized]").build());
    }
}
