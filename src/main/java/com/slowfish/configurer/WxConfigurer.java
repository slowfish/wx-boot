package com.slowfish.configurer;

import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WxConfigurer {

    @Value("${wx.app_id}")
    private String APP_ID;
    @Value("${wx.app_secret}")
    private String APP_SECRET;
    @Value("${wx.token}")
    private String TOKEN;
    @Value("${wx.aes_key}")
    private String AES_KEY;

    @Bean
    public WxMpService wxMpService() {
        WxMpInMemoryConfigStorage config = new WxMpInMemoryConfigStorage();
        config.setAppId(APP_ID); // 设置微信公众号的appid
        config.setSecret(APP_SECRET); // 设置微信公众号的app corpSecret
        config.setToken(TOKEN); // 设置微信公众号的token
        config.setAesKey(AES_KEY); // 设置微信公众号的EncodingAESKey
        WxMpService service = new WxMpServiceImpl();
        service.setWxMpConfigStorage(config);
        return service;
    }
}
