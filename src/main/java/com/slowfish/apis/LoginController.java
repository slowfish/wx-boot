package com.slowfish.apis;

import com.slowfish.aim.userlogin.service.ThirdPartTokenService;
import com.slowfish.aim.userlogin.service.UserSignInUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/login")
public class LoginController {

    @Autowired
    private ThirdPartTokenService thirdPartTokenService;
    @Autowired
    private UserSignInUpService userSignInUpService;

    @PostMapping("/regist")
    public String doRegist(String openId, String token) {
        final int flag[] = new int[1]; // 标记是否验证成功
        // 验证权限（这个验证方法需要在 service 进行实现）
        thirdPartTokenService.validatedInfo(ThirdPartTokenService.TYPE.WeChat, openId, token, (isValidated, openId1, nickname, headIconUrl) -> {
            if (isValidated) { // 如果验证通过
                // 注册
                userSignInUpService.signUpWithThirdPart(ThirdPartTokenService.TYPE.WeChat, openId1, nickname, headIconUrl);
                // 其他处理
                flag[0] = 1;
            }
        });
        return flag[0] > 0 ? "SUCCESS" : "FAILURE";
    }
}
